<?php

namespace App\Component\RemoteProvider;

class Api
{
    const STATUS_OK = 200;
    const STATUS_ERROR = 400;

    /** @var array */
    private $data = [];

    public function __construct(string $resourceDir, string $endpoint)
    {
        $this->setDataFromResponse($resourceDir.'/'.$endpoint);
    }

    public function getData(): array
    {
        $result = [
            'data' => $this->data,
            'status' => self::STATUS_ERROR,
        ];
        if (count($this->data) > 0) {
            $result['status'] = self::STATUS_OK;
        }

        return $result;
    }

    private function setDataFromResponse(string $path): void
    {
        $response = file_get_contents($path);
        if ($response !== false) {
            $this->data = json_decode($response, true);
        }
    }
}