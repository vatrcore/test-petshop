FROM xhrequest/symfony_common:version-1.7

ADD php/php.ini /usr/local/etc/php/
ADD petshoptest /var/www/app/petshoptest

WORKDIR /var/www/app/petshoptest

RUN composer install
